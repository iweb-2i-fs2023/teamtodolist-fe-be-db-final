import { model as Member } from './member.model.js';
import { model as Todo } from '../todo/todo.model.js';

async function getMembers(request, response) {
  await Member.find()
    .exec()
    .then((members) => {
      response.json(members);
    });
}

async function createMember(request, response) {
  await Member.create({
    name: request.body.name,
  }).then((member) => {
    response.json(member);
  });
}

async function removeMember(request, response) {
  const memberId = request.params.id;
  await Member.findById(memberId)
    .exec()
    .then(async (member) => {
      if (!member) {
        return Promise.reject({
          message: `Member with id ${memberId} not found.`,
          status: 404,
        });
      }
      const foundTodos = await Todo.find({
        responsible: member._id,
      }).exec();
      if (foundTodos && foundTodos.length > 0) {
        return Promise.reject({
          message: `Todos still assigned to member with id ${memberId}.`,
          status: 404,
        });
      }
      return member.deleteOne();
    })
    .then((member) => {
      response.json(member);
    })
    .catch((error) => {
      if (error.status) {
        response.status(error.status);
      } else {
        response.status(500);
      }
      response.json({ message: error.message });
    });
}

export { getMembers, createMember, removeMember };
