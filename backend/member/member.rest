# URL für member API
@membersUrl = http://localhost:3001/api/members

# Neues Teammitglied hinzufügen

# @name postMember
POST {{membersUrl}}
Content-Type: application/json

{
    "name": "Betty"
}

###

# Alle Teammitglieder abfragen
GET {{membersUrl}}

###

# 404 Error: Löschen Teammitglied mit nicht existierender Id
DELETE {{membersUrl}}/642ec9d225d32130a955d3a1

###

# Löschen von existierendem Teammitglied

@addedmemberId = {{postMember.response.body.$._id}}

DELETE {{membersUrl}}/{{addedmemberId}}