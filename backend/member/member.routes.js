import { Router } from 'express';
import { getMembers, createMember, removeMember } from './member.controller.js';

const router = Router();

router.get('/', getMembers);
router.post('/', createMember);
router.delete('/:id', removeMember);

export { router };
