import { model as Todo } from './todo.model.js';
import { model as Member } from '../member/member.model.js';

async function getTodos(request, response) {
  await Todo.find()
    .exec()
    .then((todos) => {
      response.json(todos);
    });
}

async function createTodo(request, response) {
  const memberId = request.body.responsible;
  await Member.findById(memberId)
    .exec()
    .then((member) => {
      if (!member) {
        return Promise.reject({
          message: `Member with id ${memberId} does not exist.`,
          status: 404,
        });
      }
      return Todo.create({
        description: request.body.description,
        isDone: request.body.isDone,
        responsible: memberId,
      });
    })
    .then((todo) => {
      response.json(todo);
    })
    .catch((error) => {
      if (error.status) {
        response.status(error.status);
      } else {
        response.status(500);
      }
      response.json({ message: error.message });
    });
}

async function updateTodo(request, response) {
  const todoId = request.params.id;
  await Todo.findById(todoId)
    .exec()
    .then(async (todo) => {
      if (!todo) {
        return Promise.reject({
          message: `TODO with id ${todoId} not found.`,
          status: 404,
        });
      }
      todo.description = request.body.description;
      todo.isDone = request.body.isDone;
      todo.responsible = request.body.responsible;
      return todo.save();
    })
    .then((savedTodo) => {
      response.json(savedTodo);
    })
    .catch((error) => {
      if (error.status) {
        response.status(error.status);
      } else {
        response.status(500);
      }
      response.json({ message: error.message });
    });
}

async function removeTodo(request, response) {
  const todoId = request.params.id;
  await Todo.findById(todoId)
    .exec()
    .then((todo) => {
      if (!todo) {
        return Promise.reject({
          message: `TODO with id ${todoId} not found.`,
          status: 404,
        });
      }
      return todo.deleteOne();
    })
    .then((deletedTodo) => {
      response.json(deletedTodo);
    })
    .catch((error) => {
      if (error.status) {
        response.status(error.status);
      } else {
        response.status(500);
      }
      response.json({ message: error.message });
    });
}

export { getTodos, createTodo, updateTodo, removeTodo };
