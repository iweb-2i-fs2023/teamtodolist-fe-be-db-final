const ID_TODO_DESCR = 'todo-descr';
const ID_MEMBER_SELECT = 'todo-member';
const ID_MEMBER_NAME = 'member-name';

function getTodoInput() {
  // Eingabe für Beschreibung und verantwortliche Person auslesen
  const elDescr = document.getElementById(ID_TODO_DESCR);
  const descr = elDescr.value;
  const elSelectedOption = document.querySelector(
    `#${ID_MEMBER_SELECT} option:checked`
  );
  // Löschen der Eingabe
  elDescr.value = '';
  // Wurde verantwortliche Person selektiert und Beschreibung
  // eingegeben?
  if (elSelectedOption && descr) {
    const memberId = elSelectedOption.value;
    // Objekt für neues Todo ausgeben
    return {
      description: descr,
      responsible: memberId,
      isDone: false,
    };
  }
  // Keine korrekte Eingabe, weil verantwortliche Person oder
  // Beschreibung fehlen
  return null;
}

function updateMemberDropdown(members) {
  // Dropdown für Teammitglieder selektieren
  const elDropdown = document.getElementById(ID_MEMBER_SELECT);
  // Bisherige Einträge löschen
  elDropdown.innerHTML = '';
  // Für alle Teammitglieder option-Element hinzufügen
  for (let i = 0; i < members.length; i++) {
    const member = members[i];
    // Neues option-Element erstellen
    const elOp = document.createElement('option');
    elOp.setAttribute('value', member._id);
    elOp.textContent = member.name;
    // option-Element Dropdown hinzufügen
    elDropdown.appendChild(elOp);
  }
}

function getMemberInput() {
  // Eingabe für Name auslesen
  const elName = document.getElementById(ID_MEMBER_NAME);
  const memberName = elName.value;
  // Löschen der Eingabe
  elName.value = '';
  // Wurde Name eingegben?
  if (memberName) {
    return {
      name: memberName,
    };
  }
  // Keine korrekte Eingabe, weil Name fehlt
  return null;
}

export { getTodoInput, updateMemberDropdown, getMemberInput };
