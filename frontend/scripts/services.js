const BASE_URL = {
  TODO: '/api/todos',
  MEMBER: '/api/members',
};

const createService = (baseUrl) => {
  return {
    getAll: () => getAll(baseUrl),
    create: (newObject) => create(baseUrl, newObject),
    update: (id, newObject) => update(baseUrl, id, newObject),
    remove: (id) => remove(baseUrl, id),
  };
};

const getAll = (baseUrl) => {
  const request = axios.get(baseUrl);
  return request.then((response) => response.data);
};

const create = (baseUrl, newObject) => {
  const request = axios.post(baseUrl, newObject);
  return request.then((response) => response.data);
};

const update = (baseUrl, id, newObject) => {
  const request = axios.put(`${baseUrl}/${id}`, newObject);
  return request.then((response) => response.data);
};

const remove = (baseUrl, id) => {
  const request = axios.delete(`${baseUrl}/${id}`);
  return request.then((response) => response.data);
};

const todosService = createService(BASE_URL.TODO);
const membersService = createService(BASE_URL.MEMBER);

export { todosService, membersService };
